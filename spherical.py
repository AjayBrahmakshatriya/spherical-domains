#!/usr/bin/python
import sys
if len(sys.argv)<2:
	print "Enter dom file"
	exit(-1) 

print "Parsing dom file :", sys.argv[1]

f = open(sys.argv[1], "r")
lines = f.readlines()
f.close()

#parsing domain_parameters
config_lines = []
config_flag = 0
for i in lines:
	if config_flag == 0 and i.strip() == "%config":
		config_flag = 1
		continue
	
	if config_flag == 1 and i.strip() == "%":
		break
	if config_flag == 1:
		config_lines = config_lines+[i]
#buidling config
config ={}
for i in config_lines:
	key = i.split(":")[0].strip()
	value = i.split(":")[1].strip()
	config[key]=value

if "domain" not in config or config["domain"]!="sphere":
	print "Invalid parameter for domain, please check the dom file"
	exit(-1)
if "model" not in config or config["model"]!="lat-long":
	print "Invalid parameter for mode. Presently only lat-long supported"

lat_count = int(config["lat-count"])
long_count = int(config["long-count"])
channel_count = int(config["channel-count"])
channels = []
channels_list = config["channels"].split(",")
for i in channels_list:
	i = i.strip()
	var_name = i.split("(")[0]
	var_type = i.split("(")[1].split(")")[0]
	channels += [{"name":var_name, "type":var_type}]

print channels

f = open(sys.argv[1]+".c","w")
f.write("//struct for each point in the domain\n");
f.write("typedef struct {\n");
for i in channels:
	f.write("\t"+i["type"]+" "+i["name"]+";\n")
f.write("} channel_struct;\n")
f.write("#define LAT_COUNT "+str(lat_count)+"\n");
f.write("#define LONG_COUNT "+str(long_count)+"\n");
f.write("channel_struct channels[LAT_COUNT*2+1][LONG_COUNT/2][2];\n")


f.write("void process_converge(int gen_time0){\n");
f.write("}\n");

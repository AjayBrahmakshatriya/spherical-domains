from lib import *
import sys
if len(sys.argv)>=2:
	times = int(sys.argv[1])
else:
	times = 1
N=8
A=[[[0 for _ in range(N)] for _ in range(N)] for _ in range(12)]
A[11]=[[1]]
A[10]=[[1]]

B=[[[0 for _ in range(N)] for _ in range(N)] for _ in range(12)]
B[11]=[[0]]
B[10]=[[0]]

ref=[[[0 for _ in range(N)] for _ in range(N)] for _ in range(12)]
ref[11]=[[0]]
ref[10]=[[0]]

def printA(A):
	for k in A:
		for i in k:
			for j in i:
				print j,	
			#print ",",
		print ""
def printAT(A):
	for ki in range(len(A)):
		k=A[ki]
		if ki%2==0:
			for i in k:
				for j in i:
					print j,
				print ",",
			print ""
		else:
			for i in range(len(k)):
				for j in range(len(k)):
					print k[j][i],
				print ",",
			print ""
#printA(A)
def iterate(A,B,T,ref):
	for t in range(0,T):
		B[10][0][0] = getA(A,getA0(0,0,10,N),N,ref)+getA(A,getA1(0,0,10,N),N,ref)+getA(A,getA2(0,0,10,N),N,ref)+getA(A,getA3(0,0,10,N),N,ref)+getA(A,getA4(0,0,10,N),N,ref)
		B[11][0][0] = getA(A,getA0(0,0,11,N),N,ref)+getA(A,getA1(0,0,11,N),N,ref)+getA(A,getA2(0,0,11,N),N,ref)+getA(A,getA3(0,0,11,N),N,ref)+getA(A,getA4(0,0,11,N),N,ref)
		for k in range(0,10):
			for i in range(0,N):
				for j in range(0,N):
					if i==0 and j==0:
						B[k][i][j]=getA(A,getA0(i,j,k,N),N,ref)+getA(A,getA1(i,j,k,N),N,ref)+getA(A,getA2(i,j,k,N),N,ref)+getA(A,getA3(i,j,k,N),N,ref)+getA(A,getA4(i,j,k,N),N,ref)
					else:
						
						B[k][i][j]=getA(A,getA0(i,j,k,N),N,ref)+getA(A,getA1(i,j,k,N),N,ref)+getA(A,getA2(i,j,k,N),N,ref)+getA(A,getA3(i,j,k,N),N,ref)+getA(A,getA4(i,j,k,N),N,ref)+getA(A,getA5(i,j,k,N),N,ref)
		A[10][0][0]=B[10][0][0]
		A[11][0][0]=B[11][0][0]
		for k in range(0,10):
			for i in range(0,N):
				for j in range(0,N):
					A[k][i][j]=B[k][i][j]

iterate(A,B,times,ref)
print "Processing complete"
printAT(A)
#printAT(ref)
